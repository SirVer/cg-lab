#include<graphics.h>
#include<stdio.h>
#include<math.h>

int left, top, right, bottom, x[10], y[10];

void translate(int tx, int ty)
{
	bar3d(left, top, right, bottom, 20, 1);
	getch();
	left = left + tx;
	right = right + tx;
	top = top + ty;
	bottom = bottom + ty;
	bar3d(left, top, right, bottom, 20, 1);
}
void draw1()
{
	int x1=200,y1=200,a=60,x2,y2;
	x2=(x1+(a/4)*0.707);
	y2=(y1-(a/4)*0.707);
	x[0]=x1;
	y[0]=y1;
	x[1]=x1+a;
	y[1]=y1;
	x[2]=x1+a;
	y[2]=y1+a;
	x[3]=x1;
	y[3]=y1+a;
	x[4]=x2;
	y[4]=y2;
	x[5]=x2+a;
	y[5]=y2;
	x[6]=x[5];
	y[6]=y2+a;
	line(x[0],y[0],x[1],y[1]);
	line(x[1],y[1],x[2],y[2]);
	line(x[2],y[2],x[3],y[3]);
	line(x[3],y[3],x[0],y[0]);
	line(x[0],y[0],x[4],y[4]);
	line(x[1],y[1],x[5],y[5]);
	line(x[4],y[4],x[5],y[5]);
	line(x[5],y[5],x[6],y[6]);
	line(x[1],y[1],x[5],y[5]);
	line(x[2],y[2],x[6],y[6]);
}
void draw2()
{
	line(x[0],y[0],x[1],y[1]);
	line(x[1],y[1],x[2],y[2]);
	line(x[2],y[2],x[3],y[3]);
	line(x[3],y[3],x[0],y[0]);
	line(x[0],y[0],x[4],y[4]);
	line(x[1],y[1],x[5],y[5]);
	line(x[4],y[4],x[5],y[5]);
	line(x[5],y[5],x[6],y[6]);
	line(x[1],y[1],x[5],y[5]);
	line(x[2],y[2],x[6],y[6]);
}
void rotate(float theta, int refx, int refy)
{
	int i;
	draw1();
	getch();
	theta  =  theta * 3.14/180;
	for(i=0;i<7;i++)
	{
	x[i]=refx+((x[i]-refx)*cos(theta)-(y[i]-refy)*sin(theta));
	y[i]=refy+((x[i]-refx)*sin(theta)+(y[i]-refy)*cos(theta));
	}
	draw2();
}

void main(int argc, char const *argv[])
{
	int gd=DETECT,gm,op;
	int tx, ty, theta, refx, refy;
	initgraph(&gd,&gm,"");
	left=200;
	top=200;
	right=260;
	bottom=260;
	getch();
	sscanf(argv[1], "%d", &op);
	if(op==1)
	{
		sscanf(argv[2], "%di + %dj", &tx, &ty);
		translate(tx, ty);
	}
	if(op==2)
	{
		sscanf(argv[2], "%d", &theta);
		sscanf(argv[3], "(%d,  %d)", &refx, &refy);
		rotate(theta, refx, refy);
	}
	getch();
}