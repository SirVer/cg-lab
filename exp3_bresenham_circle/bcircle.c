#include <graphics.h>
#include <conio.h>
#include <stdio.h>

void main()
{
	int gd = DETECT,gm;
	int xc, yc, r, xk, yk, pk;
	initgraph(&gd, &gm, "");
	sscanf(argv[1], "(%d, %d)", &xc, &yc);
	sscanf(argv[1], "%d", &r);
	pk = 3 - (2 * r);
	xk = 0;
	yk = r;
	while (xk <= yk)
	{
		delay(10);
		putpixel(xk + xc, yk + yc, WHITE);
		putpixel(yk + xc, xk + yc, WHITE);
		putpixel(xk + xc, -yk + yc, WHITE);
		putpixel(yk + xc, -xk + yc, WHITE);
		putpixel(-yk + xc, -xk + yc, WHITE);
		putpixel(-xk + xc, -yk + yc, WHITE);
		putpixel(-xk + xc, yk + yc, WHITE);
		putpixel(-yk + xc, xk + yc, WHITE);
		if (pk < 0)
		{
			xk = xk + 1;
			pk = pk + (4 * xk) + 6;
		}
		else
		{
			xk = xk + 1;
			yk = yk - 1;
			pk = pk + (4 * (xk - yk)) + 10;
		}
	}
	getch();
}