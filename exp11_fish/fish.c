#include <stdio.h>
#include <graphics.h>
#include <time.h>
#include <stdlib.h>

#define NO_OF_BUBBLES 15

typedef struct
{
	int x;
	int y;
} point;

void draw_fish(point mc)
{
	// mc = mass center
	int x_offset = 0, y_offset = 0, y_step, fin_offset = 0, fin_step;
	int i = 0, j, bubble_sizes[NO_OF_BUBBLES];
	point eye, tail_fin[2], side_fin[2], bubbles[NO_OF_BUBBLES];
	eye.x = mc.x - 70;
	eye.y = mc.y - 7;
	tail_fin[0].x = mc.x + 77;
	tail_fin[0].y = mc.y - 15;
	tail_fin[1].x = tail_fin[0].x;
	tail_fin[1].y = mc.y + 15;
	side_fin[0].x = mc.x - 5;
	side_fin[0].y = mc.y - 10;
	side_fin[1].x = side_fin[0].x;
	side_fin[1].y = mc.y + 10;
	for (j = 0; j < NO_OF_BUBBLES; j++)
	{
		bubbles[j].x = rand() % 640;
		bubbles[j].y = 480 - (rand() % 100);
		bubble_sizes[j] = rand() % 5;
	}
	while(i < 180)
	{
		ellipse(mc.x-x_offset, mc.y+y_offset, 30, 330, 90, 30);
		circle(eye.x-x_offset, eye.y+y_offset, 3);
		// Head-body separator
		line(mc.x-60-x_offset, mc.y+y_offset-20, mc.x-60-x_offset, mc.y+y_offset+20);
		// Tail fin outline
		line(tail_fin[0].x-x_offset, tail_fin[0].y+y_offset, tail_fin[0].x+33-x_offset, tail_fin[0].y+y_offset-15);
		line(tail_fin[1].x-x_offset, tail_fin[1].y+y_offset, tail_fin[1].x+33-x_offset, tail_fin[1].y+y_offset+12.5);
		line(tail_fin[0].x+33-x_offset, tail_fin[0].y+y_offset-15, tail_fin[0].x+33-x_offset, tail_fin[1].y+y_offset+12.5);
		// Tail fin ribs
		line(tail_fin[0].x-x_offset, tail_fin[0].y+y_offset+7.5, 630-x_offset, tail_fin[0].y+y_offset+2.5);
		line(tail_fin[0].x-x_offset, tail_fin[0].y+y_offset+15, 630-x_offset, tail_fin[0].y+y_offset+15);
		line(tail_fin[0].x-x_offset, tail_fin[1].y+y_offset-7.5, 630-x_offset, tail_fin[1].y+y_offset-2.5);
		// Side fin outline
		line(side_fin[0].x-x_offset, side_fin[0].y+y_offset, side_fin[0].x-x_offset+fin_offset, side_fin[0].y+y_offset-15);
		line(side_fin[1].x-x_offset, side_fin[1].y+y_offset, side_fin[1].x-x_offset+fin_offset, side_fin[1].y+y_offset+15);
		line(side_fin[0].x-x_offset+fin_offset, side_fin[0].y+y_offset-15, side_fin[1].x-x_offset+fin_offset, side_fin[1].y+y_offset+15);
		// Side fin ribs
		line(side_fin[0].x-x_offset, side_fin[0].y+y_offset+5, side_fin[0].x-x_offset+fin_offset, side_fin[0].y+y_offset-2.5);
		line(side_fin[0].x-x_offset, side_fin[0].y+y_offset+10, side_fin[0].x-x_offset+fin_offset, side_fin[0].y+y_offset+10);
		line(side_fin[1].x-x_offset, side_fin[1].y+y_offset-5, side_fin[1].x-x_offset+fin_offset, side_fin[1].y+y_offset+2.5);
		for (j = 0; j < NO_OF_BUBBLES; j++)
			circle(bubbles[j].x, bubbles[j].y - i, bubble_sizes[j]);
		//setcolor(YELLOW);
		//floodfill(eye.x-x_offset, eye.y+5, YELLOW);
		// Starts again when screen end is reached
		if (x_offset >= 500)
			x_offset = 0;
		if (y_offset >= 20)
			y_step = -1;
		else if (y_offset <= -20)
			y_step = 1;
		// Side fin pull motion
		if (fin_offset >= 20)
			fin_step = -1;
		// Side fin push motion
		if (fin_offset <= 0)
			fin_step = 3;
		fin_offset = fin_offset + fin_step;
		// Changes speed of movement according to fin push/pull
		if (fin_step < 0)
			x_offset = x_offset + 2;
		else if (fin_step > 0)
			x_offset = x_offset + 5;
		delay(25);
		cleardevice();
		i++;
	}
}

int main(int argc, char const *argv[])
{
	int gd = DETECT, gm;
	point mc;
	initgraph(&gd, &gm, "");
	mc.x = 520;
	mc.y = 200;
	//mc.y = 400;
	srand(time(NULL));
	draw_fish(mc);
	return 0;
}