#include <stdio.h>
#include <graphics.h>

int xmin = 100, ymin = 100, xmax = 300, ymax = 300;

int regioncode(float x, float y)
{
	int c = 0;
	if (x < xmin)
		c = c|1;
 	else if(x > xmax)
		c = c|2;
	if (y < ymin)
		c = c|8;
	else if (y > ymax)
		c = c|4;
	return c;
}

void cohensuth(float x1, float y1, float x2, float y2)
{
	int f = 0, c1, c2, c;
	float x, y;
	while(1)
	{
		c1 = regioncode(x1, y1);
		c2 = regioncode(x2, y2);
		if (c1 == 0 && c2 == 0)
		{
			f = 1;
			break;
		}
		else if ((c1&c2) != 0)
			break;
		else
		{
			if (c1 != 0)
			{
				x = x1;
				y = y1;
			}
			else
			{
				x = x2;
				y = y2;
			}
			if (x < xmin)
			{
				y = y + (y2-y1)/(x2-x1) * (xmin-x);
				x = xmin;
			}
			else if (x > xmax)
			{
				y = y + (y2-y1)/(x2-x1) * (xmax-x);
				x = xmax;
			}
			else if (y > ymax)
			{
				x = x + (x2-x1)/(y2-y1) * (ymax-y);
				y = ymax;
			}
			else if (y < ymin)
			{
				x = x + (x2-x1)/(y2-y1) * (ymin-y);
				y = ymin;
			}
			if (c1 != 0)
			{
				x1 = x;
				y1 = y;
			}
			else
			{
				x2 = x;
				y2 = y;
			}
		}
	}
	if (f == 1)
		line(x1, y1, x2, y2);
}

void main(int argc, char const *argv[])
{
	int gd = DETECT, gm;
	int n, i;
	float x[30][2], y[30][2];
	initgraph(&gd, &gm, "");
	sscanf(argv[1], "%d", &n);
	rectangle(xmin, ymin, xmax, ymax);
	for (i = 0; i < n; i++)
	{
		sscanf(argv[2+i], "%f %f %f %f", &x[i][0], &y[i][0], &x[i][1], &y[i][1]);
		line(x[i][0], y[i][0], x[i][1], y[i][1]);
	}
	getch();
	cleardevice();
	rectangle(xmin, ymin, xmax, ymax);
	for(i = 0; i < n; i++)
		cohensuth(x[i][0], y[i][0], x[i][1], y[i][1]);
	getch();
}