#include <graphics.h>

// Function for finding absolute value
int abs(int n)
{
	return (n > 0) ? n : (-1 * n);
}

// Bresenham's for m < 1
void blda_m_lt_1(int x1, int y1, int x2, int y2)
{
	int x, y, dx, dy, D, tmpx;
	dx = abs(x2 - x1);
	dy = abs(y2 - y1);
	D = 2*dy - dx;
	y = (x2 > x1) ? y1 : y2;
	x = (x2 > x1) ? x1 : x2;
	tmpx = (x2 > x1) ? x2 : x1;
	printf("%d %d, %d %d\n", x1, y1, x2, y2);
	for (; x <= tmpx; x++)
	{
		putpixel(x, y, WHITE);
		delay(1);
		if (D >= 0)
		{
			y++;
			D = D - 2*dx;
		}
		D = D + 2*dy;
	}
	printf("%d %d\n", x, y);
}

// Bresenham's for m >= 1
void blda_m_gt_1(int x1, int y1, int x2, int y2)
{
	int x, y, dx, dy, D, tmpy;
	dx = x2 - x1;
	dy = y2 - y1;
	D = 2*dx - dy;
	x = (x2 > x1) ? x1 : x2;
	y = (x2 > x1) ? y1 : y2;
	tmpy = (y2 > y1) ? y2 : y1;
	printf("%d %d, %d %d\n", x1, y1, x2, y2);
	for (; y <= tmpy; y++)
	{
		putpixel(x, y, WHITE);
		delay(1);
		if (D >= 0)
		{
			x++;
			D = D - 2*dy;
		}
		D = D + 2*dx;
	}
	printf("%d %d\n", x, y);
}

int main(int argc, char const *argv[])
{
	int gd = DETECT, gm;
	int x1, y1, x2, y2;
	float m;
	initgraph(&gd, &gm, "");
	sscanf(argv[1], "(%d, %d)", &x1, &y1);
	sscanf(argv[2], "(%d, %d)", &x2, &y2);
	m = (y2 - y1) / (x2 - x1);
	if (abs(m) < 1)
		blda_m_lt_1(x1, y1, x2, y2);
	else
		blda_m_gt_1(x1, y1, x2, y2);
	getchar();
	closegraph();
	return 0;
}
