#!/bin/bash
gcc bresenham.c -o bresenham.o -lgraph -lm
./bresenham.o "(0, 0)" "(640, 120)"
./bresenham.o "(0, 240)" "(640, 240)"
./bresenham.o "(0, 480)" "(640, 0)"
./bresenham.o "(0, 0)" "(160, 480)"
./bresenham.o "(640, 120)" "(10, 10)"