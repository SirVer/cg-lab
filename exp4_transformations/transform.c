#include <graphics.h>
#include <stdio.h>
#include <math.h>

int x_[4], y_[4];

void draw(int x[], int y[])
{
	line(x[0], y[0], x[1], y[1]);
	line(x[1], y[1], x[2], y[2]);
	line(x[2], y[2], x[3], y[3]);
	line(x[3], y[3], x[0], y[0]);
	getch();
}

void translate(int tx, int ty, int x[], int y[])
{
	int i;
	for (i = 0; i < 4; i++)
	{
		x_[i] = x[i] + tx;
		y_[i] = y[i] + ty;
	}
}

void scale(int sx, int sy, int x[], int y[], int refx, int refy)
{
	int i, tx, ty;
	tx = -refx;
	ty = -refy;
	translate(tx, ty, x, y);
	for (i = 0; i < 4; i++)
	{
		x_[i] = x_[i] * sx;
		y_[i] = y_[i] * sy;
	}
	translate(-tx, -ty, x_, y_);
}

void rotate(float theta, int x[], int y[], int refx, int refy)
{
	int i, tx, ty;
	theta  =  theta * 3.14/180;
	tx = -refx;
	ty = -refy;
	translate(tx, ty, x, y);
	for (i = 0; i < 4; i++)
	{
		x_[i] = x_[i] * cos(theta) - y_[i] * sin(theta);
		y_[i] = x_[i] * sin(theta) + y_[i] * cos(theta);
	}
	translate(-tx, -ty, x_, y_);
}

void reflect(char axis, int x[], int y[])
{
	int i, midx, midy;
	midx = getmaxx()/2;
	midy = getmaxy()/2;
	if (axis == 'x'||axis == 'X')
		for (i = 0; i < 4; i++)
		{
			x_[i] = x[i];
			y_[i] = -y[i];
		}
	else if (axis == 'y'||axis == 'Y')
		for (i = 0; i < 4; i++)
		{
			x_[i] = -x[i];
			y_[i] = y[i];
		}
	else if (axis == 'o'||axis == 'O')
		for (i = 0; i < 4; i++)
		{
			x_[i] = -x[i];
			y_[i] = -y[i];
		}
	translate(midx, midy, x_, y_);
}

void main(int argc,  char const  * argv[])
{
	int opt, i;
	int gd = DETECT, gm;
	int x[4], y[4], refx, refy;
	initgraph(&gd, &gm, "");
	for (i = 0; i < 4; i++)
		sscanf(argv[i + 1], "(%d,  %d)", &x[i], &y[i]);
	draw(x, y);
	sscanf(argv[5], "%d", &opt);
	if (opt == 1)
	{
		int tx, ty;
		sscanf(argv[6], "%di + %dj", &tx, &ty);
		translate(tx, ty, x, y);
		draw(x_, y_);
	}
	else if (opt == 2)
	{
		int sx, sy;
		sscanf(argv[6], "%di + %dj", &sx, &sy);
		sscanf(argv[7], "(%d,  %d)", &refx, &refy);
		scale(sx, sy, x, y, refx, refy);
		draw(x_, y_);
	}
	else if (opt == 3)
	{
		int theta;
		sscanf(argv[6], "%d", &theta);
		sscanf(argv[7], "(%d,  %d)", &refx, &refy);
		rotate(theta, x, y, refx, refy);
		draw(x_, y_);
	}
	else if (opt == 4)
	{
		char axis;
		int i, midx, midy;
		midx = getmaxx()/2;
		midy = getmaxy()/2;
		sscanf(argv[6], "%c", &axis);
		closegraph();
		initgraph(&gd, &gm, "");
		line(midx, 0, midx, midy * 2);
		line(0, midy, midx * 2, midy);
		translate(midx, midy, x, y);
		draw(x_, y_);
		reflect(axis, x, y);
		draw(x_, y_);
	}
}