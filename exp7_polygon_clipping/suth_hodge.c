#include <stdio.h>
#include <graphics.h>

int k;
float xmin, ymin, xmax, ymax, cords[20], m;

void clipleft(float x1, float y1, float x2, float y2)
{
	m = (y2 - y1)/(x2 - x1);
	if(x1 >= xmin && x2 >= xmin)
	{
		cords[k] = x2;
		cords[k+1] = y2;
		k += 2;
	}
	if (x1 < xmin && x2 >= xmin)
	{
		cords[k] = xmin;
		cords[k+1] = y1 + m * (xmin - x1);
		cords[k+2] = x2;
		cords[k+3] = y2;
		k+=4;
	}
	if (x1 >= xmin && x2 < xmin)
	{
		cords[k] = xmin;
		cords[k+1] = y1 + m * (xmin - x1);
		k += 2;
	}
}

void cliptop(float x1, float y1, float x2, float y2)
{
	if (y2 - y1)
		m = (x2-x1)/(y2-y1);
	if (y1 <= ymax && y2 <= ymax)
	{
		cords[k] = x2;
		cords[k+1] = y2;
		k += 2;
	}
	if (y1 > ymax && y2 <= ymax)
	{
		cords[k] = x1 + m * (ymax - y1);
		cords[k+1] = ymax;
		cords[k+2] = x2;
		cords[k+3] = y2;
		k += 4;
	}
	if (y1 <= ymax && y2 > ymax)
	{
		cords[k]=x1+m*(ymax-y1);
		cords[k+1]=ymax;
		k+=2;
	}
}

void clipright(float x1, float y1, float x2, float y2)
{
	if (x2 - x1)
		m = (y2 - y1)/(x2 - x1);
	if (x1 <= xmax && x2 <= xmax)
	{
		cords[k] = x2;
		cords[k+1] = y2;
		k += 2;
	}
	if (x1 > xmax && x2 <= xmax)
	{
		cords[k] = xmax;
		cords[k+1] = y1 + m * (xmax - x1);
		cords[k+2] = x2;
		cords[k+3] = y2;
		k += 4;
	}
	if (x1 <= xmax && x2 > xmax)
	{
		cords[k] = xmax;
		cords[k+1] = y1 + m * (xmax - x1);
		k += 2;
	}
}

void clipbottom(float x1, float y1, float x2, float y2)
{
	if (y2 - y1)
		m = (x2 - x1)/(y2 - y1);
	if (y1 >= ymin && y2 >= ymin)
	{
		cords[k] = x2;
		cords[k+1] = y2;
		k += 2;
	}
	if (y1 < ymin && y2 >= ymin)
	{
		cords[k] = x1 + m * (ymin - y1);
		cords[k+1] = ymin;
		cords[k+2] = x2;
		cords[k+3] = y2;
		k += 4;
	}
	if (y1 >= ymin && y2 < ymin)
	{
		cords[k] = x1 + m * (ymin - y1);
		cords[k+1] = ymin;
		k += 2;
	}
}

void main(int argc, char const *argv[])
{
	int gd=DETECT, gm, n, i;
	int polyy[20];
	sscanf(argv[1], "(%f, %f)", &xmin, &xmax);
	sscanf(argv[2], "(%f, %f)", &ymin, &ymax);
	sscanf(argv[3], "%d", &n);
	initgraph(&gd, &gm, "");
	for (i = 0; i < n*2; i+=2)
		sscanf(argv[4 + i/2], "(%d, %d)", &polyy[i], &polyy[i+1]);
	polyy[i] = polyy[0];
	polyy[i+1] = polyy[1];
	rectangle(xmin, ymin, xmax, ymax);
	fillpoly(n+1, polyy);
	getch();
	cleardevice();
	k = 0;
	for (i = 0; i < 2*n; i+=2)
		clipleft(polyy[i], polyy[i+1], polyy[i+2], polyy[i+3]);
	n = k/2;
	for (i = 0; i < k; i++)
		polyy[i] = cords[i];
	polyy[i] = polyy[0];
	polyy[i+1] = polyy[1];
	k = 0;
	for (i = 0; i < 2*n; i+=2)
		clipright(polyy[i], polyy[i+1], polyy[i+2], polyy[i+3]);
	n = k/2;
	for (i = 0; i < k; i++)
		polyy[i] = cords[i];
	polyy[i] = polyy[0];
	polyy[i+1] = polyy[1];
	k = 0;
	for (i = 0; i < 2*n; i+=2)
		clipbottom(polyy[i], polyy[i+1], polyy[i+2], polyy[i+3]);
	n = k/2;
	for (i = 0; i < k; i++)
		polyy[i] = cords[i];
	polyy[i] = polyy[0];
	polyy[i+1] = polyy[1];
	k = 0;
	for (i = 0; i < 2*n; i+=2)
		cliptop(polyy[i], polyy[i+1], polyy[i+2], polyy[i+3]);
	for (i = 0; i < k; i++)
		polyy[i] = cords[i];
	polyy[i] = polyy[0];
	polyy[i+1] = polyy[1];
	cleardevice();
	rectangle(xmin, ymin, xmax, ymax);
	fillpoly(k/2, polyy);
	getch();
}




















































