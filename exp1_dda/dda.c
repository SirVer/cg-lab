#include <stdio.h>
#include <graphics.h>

// Function for finding absolute value
int abs (int n)
{
	return (n > 0) ? n : (-1 * n);
}

// DDA Function for line generation
void DDA(int x1, int y1, int x2, int y2)
{
	// calculate dx & dy
	int dx = x2 - x1;
	int dy = y2 - y1;
	// calculate steps required for generating pixels
	int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);
	// calculate increment in x & y for each steps
	float xinc = dx / (float) steps;
	float yinc = dy / (float) steps;
	// Put pixel for each step
	float x = x1;
	float y = y1;
	for (int i = 0; i <= steps; i++)
	{
		putpixel(x, y, WHITE);	// put pixel at (x,y)
		x += xinc;				// increment in x at each step
		y += yinc;				// increment in y at each step
		delay(1);				// for visualization of line-
								// generation step by step
	}
}

int main(int argc, char const *argv[])
{
	int gd = DETECT, gm;
	int x1, y1, x2, y2;
	initgraph(&gd, &gm, "");
	sscanf(argv[1], "(%d, %d)", &x1, &y1);
	sscanf(argv[2], "(%d, %d)", &x2, &y2);
	DDA(x1, y1, x2, y2);
	getchar();
	closegraph();
	return 0;
}