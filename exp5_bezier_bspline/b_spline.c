#include<stdio.h>
#include<graphics.h>
#include<math.h>

int n, d = 3, uj[20] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

float f(float n)
{
	int i, a = 1;
	if (n == 0 || n == 1)
		return 1;
	for (i = 1; i <= n; i++)
		a = a * i;
	return a;
}

float bkd(int j, int d, float u)
{
	float z;
	if(d==1)
	{
		if(uj[j] < u && u < uj[j+1])
			return 1;
		else
			return 0;
	}
	z = ((u - uj[j]) / (uj[j+d-1] - uj[j])) * bkd(j, d-1, u) + ((uj[j+d] - u) / (uj[j+d] - uj[j+1])) * bkd(j+1, d-1, u);
	return z;
}

float bez(float j, float n, float u)
{
	float s,b;
	if (u == 0)
		b = pow(1-u, n-j);
	else if(1-u == 0)
		b = pow(u, j);
	else
		b = pow(u, j) * pow(1-u, n-j);
	s = f(n)/(f(j) * f(n-j)) * b;
	return s;
}

void main(int argc, char const *argv[])
{
	int gd = DETECT, gm;
	int i, x[30], y[30], j, ch = 0;
	float u, x1, y1;
	initgraph(&gd, &gm, "");
	for (n = 0; n < argc-1; n++)
	{
		sscanf(argv[n+1], "(%d, %d)", &x[n], &y[n]);
		putpixel(x[n], y[n], GREEN);
	}
	n--;
	for (i = 1; i <= n; i++)
	{
		setcolor(YELLOW);
		line(x[i-1], y[i-1], x[i], y[i]);
	}
	for (u = 3; u <= n+2; u = u + 0.01)
	{
		x1 = 0;
		y1 = 0;
		for (j = 0; j <= n; j++)
		{
			x1 = x1 + x[j] * bkd(j,d,u);
			y1 = y1 + y[j] * bkd(j,d,u);
		}
		putpixel(x1, y1, GREEN);
	}
	getch();
}
