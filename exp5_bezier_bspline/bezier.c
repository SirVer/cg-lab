#include <graphics.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>

typedef struct
{
	int x;
	int y;
} point;

int printpixel(point p)
{
	printf("(%d, %d)\n", p.x, p.y);
}

int fact(int n)
{
	if (n == 0)
		return 1;
	else
		return n * fact(n-1);
}

double C(int n, int r)
{
	return fact(n) / (fact(r) * fact(n - r));
}

point mul(double n, point p)
{
	p.x = n * p.x;
	p.y = n * p.y;
	return p;
}

point add(point p1, point p2)
{
	point p;
	p.x = p1.x + p2.x;
	p.y = p1.y + p2.y;
	return p;
}

point bezier(double t, int n, point* P)
{
	int i;
	double bez;
	point pixel;
	pixel.x = 0;
	pixel.y = 0;
	for (i = 0; i < n; i++)
	{
		if (n-1-i == 0)
			bez = C(n-1, i) * pow(t, i);
		else if (i == 0)
			bez = C(n-1, i) * pow(1-t, n-1-i);
		else
			bez = C(n-1, i) * pow(1-t, n-1-i) * pow(t, i);
		/*printf("%lf = %lf^%lf, %lf = %lf^%lf\n", pow(1-t, n-i), 1-t, n-i, pow(t, i), t, i);*/
		pixel = add(pixel, mul(bez, P[i]));
	}
	//printpixel(pixel);
	return pixel;
}

int main(int argc, char const *argv[])
{
	int gd = DETECT, gm;
	int n;
	point* P, pixel;
	double step = 0.01, t;
	initgraph(&gd, &gm, "");
	P = malloc(argc * sizeof(point));
	for (n = 0; n < argc-1; n++)
	{
		sscanf(argv[n+1], "(%d, %d)", &P[n].x, &P[n].y);
		/*printpixel(P[n]);*/
		putpixel(P[n].x, P[n].y, GREEN);
	}
	for (t = 0; t <= 1; t += step)
	{
		pixel = bezier(t, n, P);
		/*printpixel(pixel);*/
		delay(1);
		putpixel(pixel.x, pixel.y, WHITE);
	}
	getch();
	return 0;
}