#!/bin/bash
gcc bezier.c -o bezier.o -lgraph -lm
./bezier.o "(100, 100)" "(150, 150)" "(200, 100)" "(250, 150)" "(300, 100)"
./bezier.o "(100, 100)" "(180, 200)" "(200, 50)" "(220, 130)" "(300, 100)"
./bezier.o "(100, 100)" "(200, 100)" "(200, 200)" "(100, 200)"
