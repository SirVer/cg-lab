#include <stdio.h>
#include <graphics.h>
#include <time.h>
#include <stdlib.h>

#define NO_OF_RAND_INTS 500

typedef struct
{
	int x;
	int y;
} point;

int rand_ints[NO_OF_RAND_INTS];
int rand_index;

void stochastic_solid_graph(point p1, point p2, int outline_color, int fill_color)
{
	int x_direction, y_direction;
	point tmp;
	if (p1.x < p2.x)
		x_direction = 1;
	else if (p1.x > p2.x)
		x_direction = -1;
	else
		x_direction = 0;
	if (p1.y < p2.y)
		y_direction = 1;
	else if (p1.y > p2.y)
		y_direction = -1;
	else
		y_direction = 0;
	while (p1.x != p2.x || p1.y != p2.y)
	{
		int poly_coords[10];
		tmp.x = p1.x + x_direction * rand_ints[rand_index];
		tmp.y = p1.y + y_direction * rand_ints[rand_index+1];
		rand_index += 2;
		rand_index %= NO_OF_RAND_INTS;
		if (x_direction * tmp.x > x_direction * p2.x)
			tmp.x = p2.x;
		if (y_direction * tmp.y > y_direction * p2.y)
			tmp.y = p2.y;
		setcolor(fill_color);
		poly_coords[0] = p1.x;
		poly_coords[1] = p1.y;
		poly_coords[2] = tmp.x;
		poly_coords[3] = tmp.y;
		poly_coords[4] = tmp.x;
		poly_coords[5] = 480;
		poly_coords[6] = p1.x;
		poly_coords[7] = 480;
		poly_coords[8] = poly_coords[0];
		poly_coords[9] = poly_coords[1];
		fillpoly(5, poly_coords);
		setcolor(outline_color);
		line(p1.x, p1.y, tmp.x, tmp.y);
		p1 = tmp;
	}
}

int main(int argc, char const *argv[])
{
	int gd = DETECT, gm;
	int i, sun_radius;
	point mountain_coords[5], sun_center;
	initgraph(&gd, &gm, "");
	srand(time(NULL));
	for (rand_index = 0; rand_index < NO_OF_RAND_INTS; rand_index += 2)
	{
		rand_ints[rand_index] = rand() % 4;
		rand_ints[rand_index+1] = (rand() % 4) + 1;
	}
	rand_index = 0;
	for (i = 0; i < 5; i++)
	{
		mountain_coords[i].x = 0 + 160 * i;
		mountain_coords[i].y = 480 / (i%2 + 1);
	}
	sun_center.x = 320;
	sun_center.y = 420;
	while (1)
	{
		char keystroke;
		setcolor(RED);
		circle(sun_center.x, sun_center.y, 60);
		floodfill(sun_center.x, sun_center.y, RED);
		setcolor(BLACK);
		circle(sun_center.x, sun_center.y, 60);
		for (i = 0; i < 4; i++)
			stochastic_solid_graph(mountain_coords[i], mountain_coords[i+1], BLACK, BROWN);
		/*line(0, 479, 640, 479);
		setfillstyle(SOLID_FILL, BROWN);
		floodfill(80, 10, GREEN);*/
		keystroke = getch();
		floodfill(sun_center.x, sun_center.y, BLACK);
		if (keystroke == 'w')
			sun_center.y -= 10;
		else if (keystroke == 's')
			sun_center.y += 10;
		else if (keystroke == 'x')
			break;
		rand_index = 0;
	}
	return 0;
}